'use strict';

/**
 *  user-upload controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::user-upload.user-upload');
