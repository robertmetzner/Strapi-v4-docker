'use strict';

/**
 *  user-handling controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::user-handling.user-handling');
