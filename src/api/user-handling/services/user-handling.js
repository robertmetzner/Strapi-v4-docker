'use strict';

/**
 * user-handling service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::user-handling.user-handling');
