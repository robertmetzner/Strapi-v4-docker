'use strict';

/**
 * user-upload service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::user-upload.user-upload');
