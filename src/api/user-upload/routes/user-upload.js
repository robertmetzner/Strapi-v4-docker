'use strict';

/**
 * user-upload router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::user-upload.user-upload');
